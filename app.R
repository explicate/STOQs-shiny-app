library("rclipboard");

title <- "STOQs";
subtitle <- "Serializer of Tabulated Open Questionnaire specifications";

# remotes::install_github("gitlab-r-packages-mirror/psyverse");

###-----------------------------------------------------------------------------
### User interface 
###-----------------------------------------------------------------------------

ui <-
  shiny::fluidPage(
    title = paste0(title, ", ", subtitle),
    id = "mytabsetpanel",
    theme = shinythemes::shinytheme("paper"),
    
    rclipboard::rclipboardSetup(),
    
    shiny::fluidRow(

      shiny::column(
        width = 12,

        shiny::fluidRow(
          
          shiny::column(
            width = 8,
            offset = 0,
            
            shiny::h1(
              title
            ),
            
            shiny::h3(
              subtitle
            )
            
          ),
          
          shiny::column(
            width = 4,
            offset = 0,
            
            shiny::h3(
              "TOQ specification"
            ),
            
            shiny::fileInput(
              inputId = "uploadFile",
              label = "Drag and drop file here 👇, or",
              multiple = FALSE,
              accept = c(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
              buttonLabel = "Browse...",
              placeholder = "No file selected"
            ),
            
            shiny::textAreaInput(
              inputId = "gSheetURL",
              label = NULL,
              value = "",
              placeholder = "paste the URL to a Google Sheet with the TOQ specification here",
              width = "100%",
              rows = 2,
              resize = "none"
            )
            
          )
            
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 8,
            shiny::verbatimTextOutput(
              outputId = 'SOQ_spec',
              placeholder = TRUE
            )
          ),
          shiny::column(
            width = 4,
            shiny::downloadButton(
              outputId = "downloadButton",
              "Download SOQ specification"
            ),
            shiny::p(
              shiny::uiOutput("clip")
            )
          )
        )
        
      )

    ),

  tags$link(
    rel = "stylesheet", type="text/css", href="style.css"
  )
  
)

###-----------------------------------------------------------------------------
### Server
###-----------------------------------------------------------------------------

server <- function(input, output, session) {
  
  ###---------------------------------------------------------------------------
  ### Reactive objects
  ###---------------------------------------------------------------------------
  
  tempDir <- shiny::reactiveVal();
  TOQ_spec <- shiny::reactiveVal();
  SOQ_spec <- shiny::reactiveVal();
  uqid <- shiny::reactiveVal();

  ### Create temporary dir
  tempDirName <- file.path(tempfile(pattern="TOQspec-tempdir-"));
  tempDir(tempDirName);
  dir.create(tempDirName);
  
  ###---------------------------------------------------------------------------
  ### Interface stuff
  ###---------------------------------------------------------------------------
  
  output$clip <- shiny::renderUI({
    rclipboard::rclipButton(
      inputId = "clipbtn",
      label = "Copy SOQ specification",
      clipText = SOQ_spec(),
      icon = icon("clipboard")
    )
  });

  ###---------------------------------------------------------------------------
  ### Upload a file
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(input$uploadFile, {
    
    SOQ_spec(
      tryCatch(
        psyverse::toq_to_soq(
          input$uploadFile$datapath,
          returnYAML = TRUE
        ),
        error = function(e) {
          shiny::showModal(shiny::modalDialog(
            title = "Error!",
            shiny::HTML(
              paste0(
                "<p>Sorry, but I ran into the following error when ",
                "loading the TOQ specifications from the file:</p><p>",
                e$message, "</p>"
              )
            )
          ));
          return(0);
        }
      )
    );

    output$SOQ_spec <-
      shiny::renderText(SOQ_spec());

  });  
  
  
  ###---------------------------------------------------------------------------
  ### Import Google Sheet
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(input$gSheetURL, {
    
    if (nchar(input$gSheetURL) > 0) {
      
      SOQ_spec(
        tryCatch(
          psyverse::toq_to_soq(
            input$gSheetURL,
            returnYAML = TRUE
          ),
          error = function(e) {
            shiny::showModal(shiny::modalDialog(
              title = "Error!",
              shiny::HTML(
                paste0(
                  "<p>Sorry, but I ran into the following error when ",
                  "loading the TOQ specifications from the file:</p><p>",
                  e$message, "</p>"
                )
              )
            ));
            return(0);
          }
        )
      );
      
      output$SOQ_spec <-
        shiny::renderText(SOQ_spec());
      
    }
  
  });
  
  ###---------------------------------------------------------------------------
  ### Download the YAML file
  ###---------------------------------------------------------------------------
  
  output$downloadButton <- shiny::downloadHandler(
    
    filename = function() {
      return(paste0(uqid(), ".yaml"));
    },
    
    content = function(file) {

      writeLines(
        text = SOQ_spec(),
        con = file
      );
      
    }
  );
  

}

###-----------------------------------------------------------------------------
### Run the app
###-----------------------------------------------------------------------------

shinyApp(ui = ui, server = server)
